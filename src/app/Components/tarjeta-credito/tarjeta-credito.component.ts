import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tarjeta-credito',
  templateUrl: './tarjeta-credito.component.html',
  styleUrls: ['./tarjeta-credito.component.css']
})

export class TarjetaCreditoComponent implements OnInit {
  listTarjetas: any[] = [
    { titulo: 'Scarleth guzman', numeroTarjeta: '12358964', fechaExpiracion: '022/15', cvv: '123'},
    { titulo: 'Miguel Valencia', numeroTarjeta: '54896324', fechaExpiracion: '01/25', cvv: '1234'},
    { titulo: 'Miller Ponce', numeroTarjeta: '785123695', fechaExpiracion: '02/10', cvv: '1235'},
    { titulo: 'Fidelia Bernal', numeroTarjeta: '854763214', fechaExpiracion: '03/13', cvv: '1126'},
    { titulo: 'Oliver Santos', numeroTarjeta: '98524631', fechaExpiracion: '04/05', cvv: '1237'},
    { titulo: 'Geovana Guzman', numeroTarjeta: '703205698', fechaExpiracion: '05/18', cvv: '1238'},
    { titulo: 'Noelgia Guzman', numeroTarjeta: '605497860', fechaExpiracion: '06/20', cvv: '1239'},
    { titulo: 'Cristina Montecinos', numeroTarjeta: '587631205', fechaExpiracion: '07/30', cvv: '1231'},
    { titulo: 'Vidal Guzman', numeroTarjeta: '52633258', fechaExpiracion: '08/31', cvv: '1232'},
    { titulo: 'Zoe Arias', numeroTarjeta: '89654730', fechaExpiracion: '09/23', cvv: '1233'},
    { titulo: 'Alisson Guzman', numeroTarjeta: '25486310', fechaExpiracion: '07/04', cvv: '1234'},
    { titulo: 'Daniela Valencia', numeroTarjeta: '12547896', fechaExpiracion: '08/15', cvv: '1235'},
  ];
  form: FormGroup

  constructor(private fb: FormBuilder) { 
    this.form = this.fb.group({
      titulo: ['', Validators.required],
      numeroTarjeta: ['',[Validators.required, Validators.maxLength(12), Validators.minLength(12)]],
      fechaExpiracion: ['', [Validators.required, Validators.maxLength(5), Validators.minLength(5)]],
      cvv: ['',[Validators.required, Validators.maxLength(3), Validators.minLength(3)]]
    })
    
  }

  ngOnInit(): void {
    ;
  }

  agregarTarjeta(): void{

    console.log(this.form);
    const tarjeta: any ={
      titulo: this.form.get('titulo')?.value,
      numeroTarjeta: this.form.get('numeroTarjeta')?.value,
      fechaExpiracion: this.form.get('fechaExpiracion')?.value,
      cvv: this.form.get('cvv')?.value,
    }
    this.listTarjetas.push(tarjeta)
    this.form.reset();
    
  }

}
